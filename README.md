# grape_juice isometrics

## The Isometric Module For FoundryVTT 

Main features:
--------------

### Changes all grid types to Isometric per scene.

![](https://i.imgur.com/yq0gadi.png)

### Has correct measurements when in isometric, for all tools and rulers.

![](https://i.imgur.com/cr2Q29Q.png)

### Changes tokens to "stand up" giving a better visual effect

![](https://i.imgur.com/6SrUgyP.png)

### Adds better token elevation representation
![](1.mp4)

### Attaches tiles to doors and makes them disappear when opened
![](2.mp4)

### Rooms can keep their size from the outside while still having walls extend over room boundaries
![](3.mp4)

### Achieving this effect which makes your players' head go 🤯
![](4.mp4)


### This module comes with 5 artist's freebies, includes maps, tokens, and lots of love:

### [Isometric Worlds](https://patreon.com/isometricworlds), [Two Brave Puffins](https://patreon.com/twobravepuffins), [CinderLight](https://patreon.com/CinderLight), [The Iso Explorer](https://patreon.com/TheIsoExplorer),  [VividAdventureMaps](https://patreon.com/VividAdventureMaps)

## Install from foundry's module list

### If you like what you see, have any suggestions or want to chat with fellow Isometric enthusiasts, drop by my [discord](https://discord.gg/467HAfZ),

### if you Really LOVE what you see, throw a few gold coins at me over [patreon](https://patreon.com/foundry_grape_juice)



 <sup> Backgrounds, Tokens and Assets by [Isometric Worlds](https://www.patreon.com/isometricworlds) <sub>

